using System;
using Microsoft.Azure.WebJobs; 
using Microsoft.Extensions.Logging;
using Microsoft.SharePoint.Client;

namespace DemoAzureFunctionTimerJob
{
    public static class DemoAzureFunctionTimerJob
    {
        [FunctionName("DemoAzureFunctionTimerJob")]
        public static void Run([TimerTrigger("0 */5 * * * *"
#if DEBUG
            , RunOnStartup = true
#endif
        )]TimerInfo myTimer, ILogger log)
        {
            log.LogInformation($"C# Timer trigger function executed at: {DateTime.Now}");

            ContextProvider contextProvider = new ContextProvider(log);
            string webUrl = Environment.GetEnvironmentVariable("WebUrl");
            string listTitle = Environment.GetEnvironmentVariable("ListTitle");
            using (var ctx = contextProvider.GetAppOnlyClientContext(webUrl))
            {
                List list = ctx.Web.Lists.GetByTitle(listTitle);
                var query = CamlQuery.CreateAllItemsQuery();
                var items = list.GetItems(query);
                ctx.Load(items);
                ctx.ExecuteQuery();

                log.LogInformation("\t Total Document = " + items.Count);

            }
        }
    }
}
